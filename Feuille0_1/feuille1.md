# 1 - Commandes shell et processus

## 1.1 Temps consommés par les processus

Commande `time`

* **Real** is wall clock time - time from start to finish of the call. This is all elapsed time including time slices used by other processes and time the process spends blocked (for example if it is waiting for I/O to complete).

* **User** is the amount of CPU time spent in user-mode code (outside the kernel) within the process. This is only actual CPU time used in executing the process. Other processes and time the process spends blocked do not count towards this figure.

* **Sys** is the amount of CPU time spent in the kernel within the process. This means executing CPU time spent in system calls within the kernel, as opposed to library code, which is still running in user-space. Like 'user', this is only CPU time used by the process.

## 1.2 Etats d'un processus

1. On peut voir les processus avec ls commande `ps -fa`.
2. `ps -o user,pid,ppid,args` (`-o` sert à préciser le format de sortie)
3. `man ps`...
4. Les deux commandes sont assez similaires, la grosse différence est que `top` reste active si on ne l'arrête pas nous-même, alors qu'on est obligé de réutiliser `ps` régulièrement si on veut suivre l'avancement d'un ou plusieurs processus
5. Le premier programme qui démarre sous linux est `init`, il possède donc le PID n°1 et est démarré depuis le démarrage de Linux. `uptime` sert à savoir depuis quand le système est allumé, il est donc normal que le temps d'exécution du processus `init` soit égal à `uptime`.

## 1.3 Limitation des processus

`man bash` (et non `man ulimit` car c'est une commande du Bash)...

# 2 - Programmes, processus et segments mémoire

## 2.1 Quelques programmes simples

Je pense que cette partie n'a pas vraiment besoin de correction...

## 2.2 Pile d'exécution

### 2.2.1 Pile d'exécution

Programme:

```#include<stdio.h>

// First function, initialize an array of 10 int, then print it
void f1()
{
        int tab[10] = {1,2,3,4,5,6,7,8,9,10};
        for(int i =0; i < 10; i++){
                printf("%d\n", tab[i]);
        }
}

// Second function, define an array of 10 int, but doesn't initialize it, then print it
void f2()
{
        int tab[10];
        for(int i =0; i < 10; i++){
                printf("%d\n", tab[i]);
        }
}

// The main only calls f1 and f2
int main(int argc, char** argv)
{
        f1();
        f2();
}
```
Sortie:

```
1
2
3
4
5
6
7
8
9
10
1
2
3
4
5
6
7
8
9
10
```

On constate donc que l'on a affiché deux fois le contenu du tableau de la fonction `f1()`.
Pourquoi ? Les valeurs dans la pile ne sont pas remises à zéro lorsque l'on sort de la fonction `f1()`.
Ainsi, lorsqu'on définit le tableau de 10 entiers dans la fonction `f2()`, son adresse pointe sur la même adresse de départ que le tableau de la fonction `f1()`.
Et lorsque l'on itère dessus, on retrouve bien les valeurs du premier tableau.

Vérification avec l'affichage des adresses (il suffit de changer le `printf("%d\n", tab[i]);` pour `printf("%p\n", &tab[i]);`):

```
0x7ffcb0f7b470
0x7ffcb0f7b474
0x7ffcb0f7b478
0x7ffcb0f7b47c
0x7ffcb0f7b480
0x7ffcb0f7b484
0x7ffcb0f7b488
0x7ffcb0f7b48c
0x7ffcb0f7b490
0x7ffcb0f7b494

0x7ffcb0f7b470
0x7ffcb0f7b474
0x7ffcb0f7b478
0x7ffcb0f7b47c
0x7ffcb0f7b480
0x7ffcb0f7b484
0x7ffcb0f7b488
0x7ffcb0f7b48c
0x7ffcb0f7b490
0x7ffcb0f7b494
```

Cela confirme que les deux tableaux pointent vers les mêmes addresses de la pile.

NOTE: Cela arrive seulement parce que nos deux fonctions se retrouvent à être alignées dans la pile, c'est à dire qu'`f2()` prend la place de `f1()` dans la pile, et comme `f2()` ne fait que définir un tableau de 10 entiers, les valeurs sont parfaitement alignées avec celles du tableau de `f1()`.


## 2.3 Pile d'exécution(bis)

 Programme:

```
#include<stdio.h>
  
  int* f1()
  {
          int tab[10] = {1,2,3,4,5,6,7,8,9,10};
          return tab;
  }
  
  int main(int argc, char** argv)
  {
          int* tab = f1();
          for(int i = 0; i < 10; i++)
                  printf("%d\n", tab[i]);
  }

```
### gcc

Compilation avec gcc:

```
gcc pile_bis.c
pile_bis.c: In function ‘f1’:
pile_bis.c:6:9: warning: function returns address of local variable [-Wreturn-local-addr]
  return tab;
         ^
```

Le warning nous rapelle que retourner l'adresse d'une variable locale n'est pas une très bonne idée...


Exécution du programme:

```
Erreur de segmentation (core dumped)
```

Gcc va voir qu'on ne se sert pas vraiment du tableau et faire en sorte que `return tab;` retourne enfait un pointeur nul, et donc à la première utilisation de `printf`, le programme crash.


### clang

Compilation avec clang:

```
clang pile_bis.c
pile_bis.c:6:9: warning: address of stack memory associated with local variable 'tab' returned [-Wreturn-stack-address]
        return tab;
               ^~~
1 warning generated.
```

Comme gcc, clang détecte que l'on essaie de retourner l'adresse d'une variable locale.

Exécution du programme:

```
1
0
1
0
4196040
0
2073206224
32722
2073291232
32722
```

Cette fois, le compilateur ne fait pas en sorte que `f1()` renvoie un pointeur nul, donc on récupère bien une adresse, et pas de segmentation fault au niveau de `printf`.
Par contre, comme `printf` est une fonction, elle va prendre la place de `f1()` sur la pile pour faire ses affaires, et donc complètement changer les valeurs de la pile.
C'est pour cela que l'on se retrouve à afficher n'importe quoi.


## 2.3.1 Pile d'exécution (ter) - Dépassement des limites d'un tableau - Violation de segmentation

Programme :
```
#include<stdio.h>
#include<stdlib.h>


int main()
{
        int a = 1;
        int b = 1;
        int c = 1;
        int d = 1;
        int e = 1;
        int f = 1;
        int g = 1;
        int h = 1;
        int i = 1;
        int j = 1;

        int tab[10] = {2,2,2,2,2,2,2,2,2,2};

        int k = 3;
        int l = 3;
        int m = 3;
        int n = 3;
        int o = 3;
        int p = 3;
        int q = 3;
        int r = 3;
        int s = 3;
        int t = 3;

        char entry[50];

        fgets(entry, 50, stdin);

        int value = atoi(entry);
        for(int z = 0; z < value; z++)
                printf("%d\n", tab[z]);
}
```
NOTE: Il est mieux de compiler avec clang pour cet exercice, gcc fait des optimisations supplémentaires au niveau de la pile qui peuvent vous empêcher de voir ce qu'il y a à voir.
Par exemple, gcc va voir que vous n'utilisez pas les variables avant et après le tableau, et faire en sorte qu'elles ne soient jamais initialisées dans le programme.

Sortie pour i = 10 :
```
2
2
2
2
2
2
2
2
2
2
```
On a bien affiché les 10 valeurs du tableau.

Sortie pour i = 25:
```
2
2
2
2
2
2
2
2
2
4196333
0
4195632
1
1
1
1
1
1
1
1
1
1
0
4196256
```

Cette fois, on affiche d'abord les 10 valeurs de notre tableau, puis 3 valeurs (sûrement utilisées pour des questions d'alignement), puis 10 fois la valeur `1`, et encore une valeur qui semble aléatoire.
Ce qui est intéressant à remarquer ici, c'est l'affichage 10 fois de la valeur `1`. Si on revient à notre programme, on a déclaré 10 variables ayant la valeur `1`, 
un tableau de 10 entiers qui ont tous la valeur `2`, puis 10 variables qui ont la valeur `3`. Lorsque l'on essaie d'itérer sur plus de 10 "éléments de notre tableau, on se retrouve à essayer
d'afficher les valeurs présentes aux adresses mémoire au dessus de notre tableau (c'est à dire adresse de base du tableau +10, +11, +12, etc ...). Comme on affiche les variables qui ont pour valeur
`1`, cela veut dire que les adresses de ces variables sont supérieures aux adresses de notre tableau.

Or, les variables sont placées sur la pile d'exécution de façon croissante, c'est à dire que la variable `b` de notre programme va être placée sur la pile **après** la variable `a`.
Pourtant, d'après nos observations, l'adresse de notre variable `a` est plus grande que l'adresse de notre variable `b`. Ainsi, la pile d'exécution croît vers le bas en terme d'adresses mémoire.
