# 1 - Les bases du Shell

## 1.1 utilisation du manuel en ligne

Man sections :
* 1      User Commands
* 2      System Calls
* 3      C Library Functions
* 4      Devices and Special Files
* 5      File Formats and Conventions
* 6      Games et. al.
* 7      Miscellanea
* 8      System Administration tools and Daemons

## 1.2 Tube et combinaison de programmes

1. `ls` (par défaut, la commande est supposée trier par ordre alphabétique)
2. `grep '^alias' /etc/csh.cshrc | wc -l` ou `grep -c '^alias' /etc/csh.cshrc`

## 1.3 Arguments spéciaux

1. `ls -d *a*`
2. `ls -d .*`
3. `ls ~privat`

## 1.4 Variables d'environnement

1. `echo $PATH | wc -c`
2. `env` permet d'afficher toutes les variables d'environnement, pour le reste voir le man
3. 
* PATH : Spécifie les répertoires dans lesquels les exécutables sont placés pour pouvoir être utilisés sans le chemin complet jusqu'au fichier
* HOME: contient le chemin absolu vers le répertoire personnel de l'utilisateur connecté
* SHELL: Indique l'interpréteur Shell utilisé par défaut
* PS1: détermine ce qui est affiché dans le shell en début de chaque ligne


## 1.5 Paramètres et variables d'environnement d'un programme

```#include<stdio.h>

int main(int argc, char **argv, char** envp)
{
    printf("ARGS:\n");
	int i = 0;
	for(i = 0; i < argc; i++)
		printf("%s\n", argv[i]);

    printf("\nENVS:\n");
	char** env;
	for (env = envp; *env != 0; env++)
	{
		char* thisEnv = *env;
		printf("%s\n", thisEnv);    
	}
	return(0);
}
```

# 2 - Commandes shell et système d'exploitation

## 2.1 Quel est votre système d'exploitation ?

Commande `uname -a`
1. Le système d'exploitation est la dernière valeur affichée.
2. Le nom de la machine est en 2e position
Commande `who`
1. OS multi utilisateur (linux)
2. `who -u`

## 2.2 Une dernière commande pour la route

Pas de correction pour cette section, il vous suffisait de tester les différents éditeurs...
