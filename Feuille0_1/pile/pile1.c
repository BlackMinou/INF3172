#include<stdio.h>

// First function, initialize an array of 10 int, then print it
void f1()
{
	int tab[10] = {1,2,3,4,5,6,7,8,9,10};
	for(int i =0; i < 10; i++){
		printf("%d\n", tab[i]);
	}
}

// Second function, define an array of 10 int, but doesn't initialize it, then print it
void f2()
{
	int tab[10];
	for(int i =0; i < 10; i++){
		printf("%d\n", tab[i]);
	}
}

// The main only calls f1 and f2
int main(int argc, char** argv)
{
	f1();
	f2();
}
