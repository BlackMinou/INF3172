#include<stdio.h>

void f1()
{
	int tab[10] = {1,2,3,4,5,6,7,8,9,10};
	for(int i =0; i < 10; i++){
		printf("%p\n", &tab[i]);
	}
	printf("\n\n");
}

void f2()
{
	int tab[10];
	for(int i =0; i < 10; i++){
		printf("%p\n", &tab[i]);
	}
}

int main()
{
	f1();
	f2();
}
