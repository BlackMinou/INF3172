#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>


int main()
{
	// Create the pipe
	int pipefd[2];
	if(pipe(pipefd) == -1)
		perror("pipe() failed");

	int size;
	// fnctl() with F_GETPIPE_SZ for pipe size
	size = fcntl(pipefd[0], F_GETPIPE_SZ);
	printf("size of the pipe: %d\n", size);

	close(pipefd[0]);
	close(pipefd[1]);
}
