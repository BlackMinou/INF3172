#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
	// Args gestion
	int indice = 0;
	for(int i = 0; i < argc; i++){
		if(strcmp(argv[i], "|") == 0){
			argv[i] = NULL;
			indice = i;
			break;
		}
	}

	
	// First command
	char *c1 = argv[1];

	// Second commmand
	char *c2 = argv[indice +1];

	// Create the pipe
	int pipefd[2];
	if(pipe(pipefd) == -1)
		perror("pipe() failed");

	// First fork to launch the first command
	int pid = fork();
	if(pid == -1){
		perror("fork");
	} else if(pid == 0){
		// Close the read part of the pipe
		close(pipefd[0]);

		// Close stdout
		close(STDOUT_FILENO);

		// Create a copy of pipefd[1] (the write part) into the stdout file descriptor
		// i.e. redirect the output of the command into pipefd[1]
		dup2(pipefd[1], STDOUT_FILENO);

		// close superfluous file descriptor
		close(pipefd[1]);

		// Launch the command
		execvp(c1, argv + 1);
	} else {
		// Second fork to launch the second command
		pid = fork();
		if(pid == -1){
			perror("fork");
		} else if(pid == 0){
			// Close the write part of the pipe
			close(pipefd[1]);

			// Close stdin
			close(STDIN_FILENO);

			// Create a copu of pipefd[0] {the read part} into the stdin file descriptor
			// i.e. redirect the input of the command to be pipe[0]
			dup2(pipefd[0], STDIN_FILENO);
			
			// Close superfluous file descriptor
			close(pipefd[0]);

			// Launch the command
			execvp(c2, argv + indice + 1);
		}
	}

	// close both ends of the pipe in the parent
	close(pipefd[0]);
	close(pipefd[1]);
	
	// Wait for the two commands to exit
	wait(NULL);
	wait(NULL);
}
