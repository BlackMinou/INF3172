#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

// Time we're going to wait for the user to input something
int wait = 4;

static void handler(int signum)
{
	// here we're just being more and more insistent...
	if(wait == 3) {
		printf("You already wasted 4 seconds...\n");
	} else if(wait == 2) {
		printf("That's just 3 seconds left...\n");
	} else if(wait == 1) {
		printf("1 second and I stop !\n");
	} else {
		printf("That's it, you're way too long\n");
		exit(0);
	}
}


int main()
{
	struct sigaction sa;
	sa.sa_handler = handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGALRM, &sa, NULL) == -1)
		perror("sigaction() failed");

	char entry;
	
	printf("Enter Something !\n");
	int ret;
	do{
		// wait for  `wait` seconds
		ret = alarm(wait);
		// Decrement wait by 1 so next time we wait a little less
		wait--;
		errno = 0;
		// if we return from scanf with errno == EINTR, it means we got interrupted by a signal
		// That's why we're using errno in our do...while
		scanf("%s", &entry);
	}while(errno == EINTR);

	ret = alarm(0);
	printf("Good job, you had only %d seconds left\n", ret);
}
