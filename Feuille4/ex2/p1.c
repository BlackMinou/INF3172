#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void handler(int signum)
{
	// If we're here, this means the user didn't answer in time...
	printf("Way too long, buddy !\n");
	exit(0);
}


int main()
{
	
	struct sigaction sa;
	sa.sa_handler = handler;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);

	if (sigaction(SIGALRM, &sa, NULL) == -1)
		perror("sigaction() failed");

	// Set q timer to receive the signal SIGALRM
	int ret = alarm(10);
	char entry;
	
	printf("Enter Something !\n");
	
	// Wait for user input
	scanf("%s", &entry);
	
	// Get the time left before getting the signal
	ret = alarm(0);
	printf("Good job, you had only %d seconds left\n", ret);
}
