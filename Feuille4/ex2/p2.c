#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

// Hoz many time did we receive the signal SIGALRM ?
int counter = 0;

static void handler(int signum)
{
	// Firt time we receive the signal
	if(counter == 0) {
		printf("You took more than 10 seconds to answer! This is your last chance...\n");
		// We're nice, so we let him 10s again =D
		alarm(10);
		counter ++;
	// Second time ze receive the signal
	} else {
		// This time it's too late, we notify the user and quit
		printf("That's it, i'm off\n");
		exit(0);
	}
}


int main()
{
	struct sigaction sa;
	sa.sa_handler = handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	if (sigaction(SIGALRM, &sa, NULL) == -1)
		perror("sigaction() failed");

	// Set the timer
	int ret = alarm(10);
	char entry;
	
	printf("Enter Something !\n");
	// Wait for user input 
	if(scanf("%s", &entry))
		// When a system call like `scanf()` is interrupted by a signal,
		// it returns zith an error and errno is set to EINTR
		if(errno == EINTR)
			// Thus, we need to reiterate the call to `scanf()`
			// this is the second chance for the user
			scanf("%s", &entry);
	
	
	// How much time did we have left before the end of the last timer ?
	ret = alarm(0);
	printf("Good job, you had only %d seconds left\n", ret);
}
