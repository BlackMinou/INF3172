#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// handler wchich will be called when receiving a SIGINT
void handler(int signum)
{
	psignal(signum, "catched");
	exit(0);
}


int main()
{
	// Our struct needed for sigaction()
	struct sigaction sa;
	// Set the function to be called zhen receiving the signal
	sa.sa_handler = handler;

	// Initialize sa_mask to empty (see man)
	sigemptyset(&sa.sa_mask);

	// Initialize the flags to 0
	// NOTE: we need to do that because on some system
	// sa_flags has a value when declaring the struct sigaction
	sa.sa_flags = 0;

	// Actually register our handler to be called when receiving a SIGINT
	if (sigaction(SIGINT, &sa, NULL) == -1)
		perror("sigaction() failed");

	// Print the PID of the program
	// (just so it's easier to send a signal =D)
	printf("PID: %d\n", getpid());

	while(1){
		// do nothing, wait for signal
		sleep(10);
	}
}
