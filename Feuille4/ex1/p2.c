#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
	struct sigaction sa;

	// if we set sa_handler to SIG_IGN, the signal will be ignored
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction(SIGINT, &sa, NULL) == -1)
		perror("sigaction() failed");

	printf("PID: %d\n", getpid());

	while(1){
		// do nothing, wait for signal
		sleep(10);
	}
}
