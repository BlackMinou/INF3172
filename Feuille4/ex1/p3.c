#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// How many times did we receive the signal ?
int counter = 0;

// default action we need to save to go back to default behaviour
struct sigaction dflt;

static void handler(int signum)
{
	counter++;
	psignal(signum, "catched");
	// We handled the signal 2 times, noz ze go back to default behaviour
	if(counter == 2)
		sigaction(SIGINT, &dflt, NULL);
}


int main()
{
	struct sigaction sa;
	
	sa.sa_handler = handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	// By passing `&dflt` to sigaction as its 3rd argument,
	// we save the old behavior while setting the new one
	if (sigaction(SIGINT, &sa, &dflt) == -1)
		perror("sigaction() failed");

	printf("PID: %d\n", getpid());

	while(1){
		// do nothing, wait for signal
		sleep(10);
	}
}
