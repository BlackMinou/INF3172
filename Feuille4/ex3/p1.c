#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int child_terminated = 0;

static void handler(int signum)
{
	psignal(signum, "catched");
	child_terminated ++;
	// If our two children are terminated exit
	if(child_terminated ==2)
		exit(0);
}


int main()
{
	// Create first child
	int pid = fork();
	if(pid == -1)
		perror("fork failed");
	else if(pid == 0){
		exit(0);
	}else
		printf("created the first child\n");
	
	// Create second child
	pid = fork();
	if(pid == -1)
		perror("fork failed");
	else if(pid == 0){
		exit(0);
	} else
		printf("created the second child\n");

	// Register our handler for child termination
	struct sigaction sa;
	sa.sa_handler = handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGCHLD, &sa, NULL) == -1)
		perror("sigaction() failed");

	// Wait for the children to terminate
	while(1){
		// do nothing, wait for signal
		sleep(10);
	}
}


// NOTE: since we register our signal handler after creating our children,
// we can either receive onem two, or none of the termination signals if 
// the chidlren terminate before our call to sigaction().
