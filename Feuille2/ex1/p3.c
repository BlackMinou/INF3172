#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include <errno.h>
#include <string.h>

int main()
{
	for(int i = 0; i < 10; i++){
		int pid = fork();
		// Error gestion, fork returns -1 if an error occured
		if( pid == -1 )
			printf("Error in fork(): %s\n", strerror(errno));
		else if(pid == 0){
			printf("Je suis le numéro %d, mon PID est %d et mon père est %d\n", i, getpid(), getppid());
			// This is where we could break our computer.
			// If we don't exit in the childs, they continue the `for`
			// and you end up with wha tis called a "fork bomb",
			// spawning processes until your OS can no longer handle them and crashes
			exit(0);
		}
	}
	return 0;
}
