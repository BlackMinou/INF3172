#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>

int main()
{
	for(int i = 0; i < 10; i++){
		int pid = fork();
		if(pid == 0){
			printf("Je suis le numéro %d, mon PID est %d et mon père est %d\n", i, getpid(), getppid());
			exit(0);
		}
		//sleep(1);
	}
	while(1) pause();
	return 0;
}
