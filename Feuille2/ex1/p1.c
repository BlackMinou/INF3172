#include<stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

int main()
{
	printf("mon PID est %d\n", getpid());
	int pid = fork();
	// Error gestion, fork returns -1 if an error occured
	if( pid == -1 )
		printf("Error: %s\n", strerror(errno));
	else if(pid == 0)
		printf("je suis le fils et mon PID est %d\n", getpid());
	else
		printf("je suis le père et mon PID est %d\n", getpid());	
}
