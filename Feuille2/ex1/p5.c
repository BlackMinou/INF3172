#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include<sys/wait.h>
#include<string.h>
#include<errno.h>

int main()
{
	int pids[10];
	int status;
	for(int i = 0; i < 10; i++){
		int pid = fork();
		pids[i] = pid;
		if(pid == -1)
			printf("Error in fork(): %s", strerror(errno));
		else if(pid == 0){
			printf("Je suis le numéro %d, mon PID est %d et mon père est %d\n", i, getpid(), getppid());
			exit(0);
		}
	}

	for(int i = 0; i < 10; i++){
		int ret = waitpid(pids[i], &status, NULL);
		if(ret == -1)
			printf("Error in waitpid(): %s", strerror(errno));
		else
			printf("le processus n°%d, PID %d, vient de terminer\n", i, pids[i]);
	}
		return 0;
}
