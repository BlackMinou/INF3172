# Exercices relatifs à l'utilisation du fork()

## 1

Programme:

```
#include<stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

int main()
{
        printf("mon PID est %d\n", getpid());
        int pid = fork();
        // Error gestion, fork returns -1 if an error occured
        if( pid == -1 )
                printf("Error in fork(): %s\n", strerror(errno));
        else if(pid == 0)
                printf("je suis le fils et mon PID est %d\n", getpid());
        else
                printf("je suis le père et mon PID est %d\n", getpid());
}

```

## 2

```
#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
int main (void) {
        if(fork()) fork ();
        printf("Je suis %d fils de %d.\n" ,getpid(), getppid());
        return 0;
}

```

Sortie:

```
Je suis 6042 fils de 3727.
Je suis 6043 fils de 6042.
Je suis 6044 fils de 6042.
```

Explication:

`if(fork())` va être vrai uniquement au niveau du parent (rappel: `fork()` retourne 0 dans l'enfant, et le pid de l'enfant dans le parent), donc seul le processus père va reéxecuter le second `fork()`.
On se retrouve donc avec le processus père qui exécute deux fois `fork()`, puis tout le monde exécute le `printf`. Le processus père a alors deux fils.

On voit bien ce comportement grâce à la sortie du programme: `6042` est le processus lancé depuis votre terminal (`3727`), puis `6043` et `6044` sont les enfants de `6042`.

## 3

Programme:
```
#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include <errno.h>
#include <string.h>

int main()
{
        for(int i = 0; i < 10; i++){
                int pid = fork();
                // Error gestion, fork returns -1 if an error occured
                if( pid == -1 ) 
                        printf("Error in fork(): %s\n", strerror(errno));
                else if(pid == 0){
                        printf("Je suis le numéro %d, mon PID est %d et mon père est %d\n", i, getpid(), getppid());
                        // This is where we could break our computer.
                        // If we don't exit in the childs, they continue the `for`
                        // and you end up with what is called a "fork bomb",
                        // spawning processes until your OS can no longer handle them and crashes
                        exit(0);
                } 
        } 
        return 0;
}
```

Sortie:
```
Je suis le numéro 0, mon PID est 6156 et mon père est 6155
Je suis le numéro 1, mon PID est 6157 et mon père est 6155
Je suis le numéro 2, mon PID est 6158 et mon père est 6155
Je suis le numéro 3, mon PID est 6159 et mon père est 6155
Je suis le numéro 7, mon PID est 6163 et mon père est 6155
Je suis le numéro 4, mon PID est 6160 et mon père est 6155
Je suis le numéro 5, mon PID est 6161 et mon père est 1
Je suis le numéro 6, mon PID est 6162 et mon père est 1
Je suis le numéro 8, mon PID est 6164 et mon père est 1
Je suis le numéro 9, mon PID est 6165 et mon père est 1
```

NOTE: Certains processus enfants se retrouvent rattachés au processus `init` (pid = 1) car le processus père `6155` s'est terminé avant qu'ils soient complètement lancés.

## 4

Il suffisait de rajouter `while(1) pause();` avant le `return 0;` dans le programme précédent.

On liste nos processus avec `ps -la`:
```
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
0 S  1000  6192  3727  0  80   0 -  1055 pause  pts/1    00:00:00 a.out
1 Z  1000  6193  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
1 Z  1000  6194  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
1 Z  1000  6195  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
1 Z  1000  6196  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
1 Z  1000  6197  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
1 Z  1000  6198  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
1 Z  1000  6199  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
1 Z  1000  6200  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
1 Z  1000  6201  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
1 Z  1000  6202  6192  0  80   0 -     0 -      pts/1    00:00:00 a.o <defunct>
```

On voit qu'au niveau de la deuxième colonne `S`, les processus enfant de `a.out` sont marqués avec l'état `Z` pour Zombie. Ils se sont terminés, mais pas leur parent.
Un processus tombe dans l'état zombie lorqu'il s'est achevé, mais qu'il possède encore un identifiant de processus (PID). Tant que le processus père n'utilise pas `wait()` ou `waitpid()`
pour attester de la fin de ses processus enfants, ils restent dans la table des processus dans l'état zombie.


## 5

Programme:
```
#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include<sys/wait.h>
#include<string.h>
#include<errno.h>

int main()
{
        int pids[10];
        int status;
        for(int i = 0; i < 10; i++){
                int pid = fork();
                pids[i] = pid;
                if(pid == -1)
                        printf("Error in fork(): %s", strerror(errno));
                else if(pid == 0){
                        printf("Je suis le numéro %d, mon PID est %d et mon père est %d\n", i, getpid(), getppid());
                        exit(0);
                }
        }

        for(int i = 0; i < 10; i++){
                int ret = waitpid(pids[i], &status, NULL);
                if(ret == -1)
                        printf("Error in waitpid(): %s", strerror(errno));
                else
                        printf("le processus n°%d, PID %d, vient de terminer\n", i, pids[i]);
        }
                return 0;
}

```

Sortie:

```
Je suis le numéro 0, mon PID est 6442 et mon père est 6441
Je suis le numéro 1, mon PID est 6443 et mon père est 6441
Je suis le numéro 2, mon PID est 6444 et mon père est 6441
Je suis le numéro 4, mon PID est 6446 et mon père est 6441
Je suis le numéro 7, mon PID est 6449 et mon père est 6441
Je suis le numéro 5, mon PID est 6447 et mon père est 6441
Je suis le numéro 3, mon PID est 6445 et mon père est 6441
le processus n°0, PID 6442, vient de terminer
Je suis le numéro 8, mon PID est 6450 et mon père est 6441
le processus n°1, PID 6443, vient de terminer
le processus n°2, PID 6444, vient de terminer
Je suis le numéro 6, mon PID est 6448 et mon père est 6441
le processus n°3, PID 6445, vient de terminer
le processus n°4, PID 6446, vient de terminer
le processus n°5, PID 6447, vient de terminer
Je suis le numéro 9, mon PID est 6451 et mon père est 6441
le processus n°6, PID 6448, vient de terminer
le processus n°7, PID 6449, vient de terminer
le processus n°8, PID 6450, vient de terminer
le processus n°9, PID 6451, vient de terminer
```

NOTE: C'est normal que la sortie soit mélangée, tous les processus écrivent dans la sortie standard en même temps...

Comme expliqué à la fin de la question précédente, le but d'attendre la fin des processus enfant est qu'ils ne soient plus zombie.
On pourrait remettre un `while(1) pause();` à la fin de notre programme pour vérifier que le processus père fonctionne encore
mais que les enfants ne sont pas zombies à l'aide de la commande `ps -la`.


# 2 Exercices relatifs à l'utilisation des exec()


## 1

Programme:
```
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

int main(int argc, char** argv)
{
        execl("/bin/ls", "ls", "-l", NULL);
        // execl() return only if an error occured
        perror("Error in execl()");
        return 0;
}

```

## 2

Programme:
```
#include<unistd.h>
#include<stdio.h>
#include<sys/wait.h>
#include<errno.h>

int main(int argc, char** argv)
{
        int status;
        int pid = fork();
        if(pid == -1)
                perror("echec du fork");
        else if(pid == 0){
                execl("/bin/ls", "ls", "-l", NULL);
				// Next line reached only if there was an error in execl()                
				perror("echec de execl()");
        }else{
                int ret = waitpid(pid, &status, 0);
                if( ret == -1 )
                        perror("echec de waitpid");
                else
                        printf("le execl() est terminé\n");
        }
        return 0;
}

```

## 3

Programme:
```
#include<unistd.h>
#include<stdio.h>
#include<sys/wait.h>

int main()
{
        int status;


        // We could do it in a for() loop, but it's way simpler this way

        int pid1 = fork();
        if(pid1 == -1)
                perror("Echec du premier fork()");
        else if(pid1 == 0){     
                execl("/bin/ls", "ls", "-l", NULL);
                perror("Echec de execl()");
        }

        int pid2 = fork();
        if(pid2 == -1)
                perror("Echec du deuxième fork()");
        else if (pid2 == 0){    
                execlp("ls", "ls", "-l", NULL);
                perror("Echec de execlp()");
        }

        int pid3 = fork();
        char *const parmList[] = {"ls","-l", NULL};
        if(pid3 == -1)
                perror("Echec du troisième fork()");
        else if(pid3 == 0){
                execv("/bin/ls", parmList);
                perror("Echec de execv");
        }

        int pid4 = fork();
        if(pid4 == -1)
                perror("Echec du quatrième fork()");
        else if(pid4 == 0){
                execvp("ls", parmList);
                perror("Echec de execvp");
        }

        int ret1 = waitpid(pid1, &status, 0);
        if(ret1 == -1)
                perror("Erreur dans le waitpid() du premier processus enfant");
        int ret2 = waitpid(pid2, &status, 0);
        if(ret2 == -1)
                perror("Erreur dans le waitpid() du deuxième processus enfant");
        int ret3 = waitpid(pid3, &status, 0);
        if(ret3 == -1)
                perror("Erreur dans le waitpid() du troisième processus enfant");
        int ret4 = waitpid(pid4, &status, 0);
				if(ret4 == -1)
                perror("Erreur dans le waitpid() du quatrième processus enfant");
        return 0;
}
```

## 4

Programme:
```
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>

int main(int argc, char** argv)
{
        int times = atoi(argv[1]);
        char c[3];
        if(times < 10){
                times += 1;
                int ret = sprintf(c, "%d", times);
                if(ret < 0)
                        perror("Echec de sprintf()");
                execl("./p4", "p4", c, NULL);
                perror("Echec de execl");
        }
        return 0;
}

```
