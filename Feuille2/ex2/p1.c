#include <unistd.h>
#include <stdio.h>
#include <errno.h>

int main(int argc, char** argv)
{
	execl("/bin/ls", "ls", "-l", NULL);
	// execl() return only if an error occured
	perror("Error in execl()");
	return 0;
}
