#include<unistd.h>
#include<stdio.h>
#include<sys/wait.h>

int main()
{
	int status;


	// We could do it in a for() loop, but it's way simpler this way

	int pid1 = fork();
	if(pid1 == -1)
		perror("Echec du premier fork()");
	else if(pid1 == 0){	
		execl("/bin/ls", "ls", "-l", NULL);
		perror("Echec de execl()");
	}

	int pid2 = fork();
	if(pid2 == -1)
		perror("Echec du deuxième fork()");
	else if (pid2 == 0){	
		execlp("ls", "ls", "-l", NULL);
		perror("Echec de execlp()");
	}

	int pid3 = fork();
	char *const parmList[] = {"ls","-l", NULL};
	if(pid3 == -1)
		perror("Echec du troisième fork()");
	else if(pid3 == 0){
		execv("/bin/ls", parmList);
		perror("Echec de execv");
	}

	int pid4 = fork();
	if(pid4 == -1)
		perror("Echec du quatrième fork()");
	else if(pid4 == 0){
	       	execvp("ls", parmList);
		perror("Echec de execvp");
	}

	int ret1 = waitpid(pid1, &status, 0);
	if(ret1 == -1)
		perror("Erreur dans le waitpid() du premier processus enfant");
	int ret2 = waitpid(pid2, &status, 0);
	if(ret2 == -1)
		perror("Erreur dans le waitpid() du deuxième processus enfant");
	int ret3 = waitpid(pid3, &status, 0);
	if(ret3 == -1)
		perror("Erreur dans le waitpid() du troisième processus enfant");
	int ret4 = waitpid(pid4, &status, 0);
	if(ret4 == -1)
		perror("Erreur dans le waitpid() du quatrième processus enfant");
	return 0;
}
