#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>

int main(int argc, char** argv)
{
	int times = atoi(argv[1]);
	char c[10];
	if(times < 10){
		times += 1;
		int ret = sprintf(c, "%d", times);
		if(ret < 0)
			perror("Echec de sprintf()");
	       	execl("./p4", "p4", c, NULL);
		perror("Echec de execl");
	}
	return 0;
}
