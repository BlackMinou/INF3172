#include<unistd.h>
#include<stdio.h>
#include<sys/wait.h>
#include<errno.h>

int main(int argc, char** argv)
{
	int status;
	int pid = fork();
	if(pid == -1)
		perror("echec du fork");
	else if(pid ==0){
		execl("/bin/ls", "ls", "-l", NULL);
		perror("echec de execl()");

	}else{
		int ret = waitpid(pid, &status, 0);
		if( ret == -1 )
			perror("echec de waitpid");
		else
			printf("le execl() est terminé\n");
	}
	return 0;
}
