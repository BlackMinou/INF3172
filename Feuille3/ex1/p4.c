#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include <string.h>

// Function used to compare strings and sort them (for `qsort()`)
int cmp(const void* a, const void* b)
{
	    return strcmp(*((char**) a), *((char**) b));
}

int main(int argc, char** argv)
{

	// Files that we need to process
	char* file_names[argc - 1];
	// If no particular file is specified, use the current directory
	if(argc < 2){
		char cwd[1024];
   		if (getcwd(cwd, sizeof(cwd)) != NULL)
			file_names[0] = cwd;
      		else
	             perror("getcwd() error");
	}else if(argc >= 2)
		for(int i = 0; i < argc; i++)
			file_names[i] = argv[i + 1];

	// Sort the names, but only if we have more than one file !
	if((argc - 1) > 1)	
		qsort(file_names, argc - 1, sizeof *file_names, cmp);

	// Iterate through the files
	for(int i = 0; i < argc - 1; i++){
		// If it's a hidden file, don't show it	
		if(file_names[i][0] == '.') continue;

		// Get the `stat` structure to show the stats of the file
		struct stat fileStat; 
		if(stat(file_names[i],&fileStat) < 0){
			perror("error opening the file");
			return 1;
		}
	 
		printf("Information for %s\n",file_names[i]);
		printf("---------------------------\n");
		printf("File inode: \t\t%ld\n",fileStat.st_ino);
			
		printf("File Type: \t\t");

		// Flags for further checks
		int is_directory = 0;
		int is_symlink = 0;

		// Show the type of file
		switch (fileStat.st_mode & S_IFMT) {
			case S_IFBLK:  printf("block device\n");		break;
			case S_IFCHR:  printf("character device\n");		break;
			case S_IFDIR:  printf("directory\n");			is_directory = 1;	break;
			case S_IFIFO:  printf("FIFO/pipe\n");			break;
			case S_IFLNK:  printf("symlink\n");			is_symlink = 1;		break;
			case S_IFREG:  printf("regular file\n");		break;
			case S_IFSOCK: printf("socket\n");			break;
			default:       printf("unknown?\n");			break;
		}
	
		// Show the permissions of the file
		printf("File Permissions: \t");
		printf( (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
		printf( (fileStat.st_mode & S_IRUSR) ? "r" : "-");
		printf( (fileStat.st_mode & S_IWUSR) ? "w" : "-");
		printf( (fileStat.st_mode & S_IXUSR) ? "x" : "-");
		printf( (fileStat.st_mode & S_IRGRP) ? "r" : "-");
		printf( (fileStat.st_mode & S_IWGRP) ? "w" : "-");
		printf( (fileStat.st_mode & S_IXGRP) ? "x" : "-");
		printf( (fileStat.st_mode & S_IROTH) ? "r" : "-");
		printf( (fileStat.st_mode & S_IWOTH) ? "w" : "-"); 
		printf( (fileStat.st_mode & S_IXOTH) ? "x" : "-");
		printf("\n");
			
		printf("Number of Links: \t%ld\n",fileStat.st_nlink);
	
		// Show the uid and gid, converted into strings (Question 2)
		struct passwd* pwd = getpwuid(fileStat.st_uid);
		printf("UID: \t\t\t%s\n", pwd->pw_name);
		struct group* gr = getgrgid(fileStat.st_gid);
		printf("GID: \t\t\t%s\n", gr->gr_name);
	
		printf("File Size: \t\t%ld bytes\n",fileStat.st_size);
		// Last modification in human readable format (with `ctime()`)
		printf("Last modification: \t%s", ctime(&fileStat.st_mtime));
		printf("The file %s a symbolic link\n\n", (S_ISLNK(fileStat.st_mode)) ? "is" : "is not");
	
		// Use our `is_directory` flag and do the work if the fileis a directory
		if(is_directory){
			printf("Directory content:\n");
			// Open the directory
			DIR *dir = opendir(file_names[i]);
			struct dirent* entry;
			if(dir != NULL){
				while(entry = readdir(dir)){
					// Skip `.`, `..` and hidden files
					if(entry->d_name[0] == '.') continue;
					// With a function, we could show the infos of the files in the directory recursively
					// But here, we just show the names of the files contained in `dir`
					printf("%s\n", entry->d_name);
				}
				// Don't forget to close !
				closedir(dir);
			}else{
				perror("failed to open dir");
			}
		}
	
		// Use our `is_symlink` flag to show other infos if the file is a symbolik link
		if(is_symlink){
			printf("Linked to:\n");

			struct stat sb;
           		char *linkname;
           		ssize_t r;

			// `lstat()` returns infos about the symbolic link itself, not the file that it refers to
           		if (lstat(file_names[i], &sb) == -1) {
           		    perror("lstat");
           		    exit(EXIT_FAILURE);
           		}

           		linkname = malloc(sb.st_size + 1); // + 1 because null byte.
           		if (linkname == NULL) {
           		    fprintf(stderr, "insufficient memory\n");
           		    exit(EXIT_FAILURE);
           		}

			// Place the contents of the symbolik link into `linkname`
           		r = readlink(file_names[i], linkname, sb.st_size + 1);

           		if (r == -1) {
               			perror("readlink");
               			exit(EXIT_FAILURE);
           		}

           		if (r > sb.st_size) {
              			fprintf(stderr, "symlink increased in size ""between lstat() and readlink()\n");
               			exit(EXIT_FAILURE);
           		}

           		linkname[r] = '\0'; // readlink() does not append a null byte to buf :(

			// Show where the symlink points to
           		printf("'%s' points to '%s'\n", file_names[i], linkname);
		}
	}
	return 0;
}
