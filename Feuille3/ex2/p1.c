#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <utime.h>

int main(int argc, char** argv){

	if(argc < 3)
		printf("This program need at least 2 args");

	// Source file
	char* name = argv[1];

	// Open file
	int file = open(argv[1], O_RDONLY);
	if( file == -1){
		perror("Error opening the file");
		return 1;
	}

	// Name of the target
	char* copy_name = argv[2];

	// We need the stats of the file to know if it's a directory
	// and to use `chmod()` later
	struct stat fileStat;
	stat(name, &fileStat);


	// Open the copy with the flags to create it 
	// We don't use the `mode` arg because it's optional
	// and we use `chmod()` later
	int copy = open(copy_name, O_CREAT | O_WRONLY | O_EXCL);
	if(copy == -1){
		perror("Error opening the copy");
		close(file);
		return 1;
	}

	// Set the sources permissions to the copy
	// We need to use this because the optional argument `mode` of `open()`
	// makes it so the permissions are `(mode & ~umask)` which means that it 
	// depends on `umask` and could be different from the source file
	if(chmod(copy_name, fileStat.st_mode))
		perror("chmod");
	
	// Set the sources permissions to the copy
	// We need to use this because the optional argument `mode` of `open()`
	// makes it so the permissions are `(mode & ~umask)` which means that it 
	// depends on `umask` and could be different from the source file
	struct utimbuf new_times;
	new_times.actime = fileStat.st_atime;
	new_times.modtime = fileStat.st_mtime;
	if(utime(copy_name, &new_times))
		perror("utime");

	// Buffer for reading/writing from/to file/copy
	char buf[4096];
	ssize_t nread;
	
	// We do the assignation inside the `while()`
	// NOTE: May not work with all compilers
	// `read()` returns 0 when EOF is reached, so it will automatically stop looping
	// The value returned by `read()` on success is the number of bytes read
	while(nread = read(file, buf, sizeof buf)){
		if(nread == -1){
			// We need to close the file descriptors
			perror("Failed to read from source file");
			close(file);
			close(copy);
			return 1;
		}

		// Write to the copy
		ssize_t nwrite = write(copy, buf, nread);
		if(nwrite == -1){
			perror("Failed to write to the copy file");
			break;
		}
	}
	// Again, don't forget to close the file descriptors
	// when we're done with them
	close(file);
	close(copy);
	return 0;
}
