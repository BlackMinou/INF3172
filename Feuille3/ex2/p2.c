#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <utime.h>
#include <stdlib.h>
#include <errno.h>


void copy_file(char*,char*);
void copy_directory(char*, char*);

// This function copies a file `name` into another file `to`
// If the file is a directory, we use `copy_directory()` instead
// to recursively copy all files
void copy_file(char* name, char* to){
	
	// Names for our files
	char source_name[500] = "", copy_name[500] = "";
	strcat(source_name, name);
	strcat(copy_name, to);

	// We need the stats of the file to know if it's a directory
	// and to use `chmod()` later
	struct stat file_stat;
	if(stat(source_name, &file_stat) < 0)
		perror("stat");
	
	// Check if the file is a directory
	if((file_stat.st_mode & S_IFMT) == S_IFDIR){
		copy_directory(source_name, copy_name);
	} else {
		
		// Open file
		int file = open(source_name, O_RDONLY);
		if( file == -1){
			perror("Error opening the file");
			exit(1);
		}
		
		// Open the copy with the flags to create it 
		// We don't use the `mode` arg because it's optional
		// and we use `chmod()` later
		int copy = open(copy_name, O_CREAT | O_WRONLY | O_EXCL);
		if(copy == -1){
			perror("Error opening the copy");
			close(file);
			exit(1);
		}

		// Set the sources permissions to the copy
		// We need to use this because the optional argument `mode` of `open()`
		// makes it so the permissions are `(mode & ~umask)` which means that it 
		// depends on `umask` and could be different from the source file
		if(chmod(copy_name, file_stat.st_mode))
			perror("chmod");

		// Set the access and modification times to the copy
		struct utimbuf new_times;
		new_times.actime = file_stat.st_atime;
		new_times.modtime = file_stat.st_mtime;
		if(utime(copy_name, &new_times))
			perror("utime");
	
		// Buffer for reading/writing from/to file/copy
		char buf[4096];
		ssize_t nread;
		
		// We do the assignation inside the `while()`
		// NOTE: May not work with all compilers
		// `read()` returns 0 when EOF is reached, so it will automatically stop looping
		// The value returned by `read()` on success is the number of bytes read
		while(nread = read(file, buf, sizeof buf)){
			if(nread == -1){
				perror("Failed to read from source file");
				// We need to close the file descriptors
				close(file);
				close(copy);
				exit(1);
			}
			
			// Write to the copy
			ssize_t nwrite = write(copy, buf, nread);
			if(nwrite == -1){
				perror("Failed to write to the copy file");
				break;
			}
		}
		// Again, don't forget to close the file descriptors
		// when we're done with them
		close(file);
		close(copy);
		return;
	}
}

// This function copies a directory and call `copy_file()` for each file it contains
void copy_directory(char* name, char* to){
       	
	// Open the directory
       	DIR *dir = opendir(name);
        if(dir != NULL){
		char* copy_name = to;
		
		// Get the stats of the directory to use `chmod()`
		struct stat dir_stat;
		if(stat(name, & dir_stat))
				perror("stat dir");
	
		// Create the copy directory
		// NOTE: for mkdir, the argument `mode` is mandatory
		// But the effective permissions granted to the created dir 
		// are `(mode & ~umask & 0777)` so we have the same problem as with `open()`
		int copy_dir = mkdir(copy_name, dir_stat.st_mode);
		if(copy_dir == -1)
			perror("mkdir");
 		
	 	// Set the sources permissions to the copy
		if(chmod(copy_name, dir_stat.st_mode))
			perror("chmod");
	
		// Set the access and modification times to the copy
		struct utimbuf new_times;
		new_times.actime = dir_stat.st_atime;
		new_times.modtime = dir_stat.st_mtime;
		if(utime(copy_name, &new_times))
			perror("utime");

		// Now that we created the copy of the directory, we want to 
		// copy all files recursively ! 
        	struct dirent* entry;

		// We also do the assignation inside the while here
		// `readdir()` returns NULL if the end of the directory is reached
		// OR if an error hapenned, we can use `errno` to check if there was an error !
		errno = 0;
		while(entry = readdir(dir)){
                	if(entry->d_name[0] == '.') continue;
			// We need to concat the names so we don't mess up the target and destination paths
			char from[500] = "";
			strcat(from, name);

			// Do we have to add the `/` at the end ?
			if(name[strlen(name) - 1] != '/')
				strcat(from, "/");
			strcat(from, entry->d_name);
			
			char dest[500] = "";
			strcat(dest, copy_name);
			if(dest[strlen(dest) - 1] != '/')
				strcat(dest, "/");
			strcat(dest, entry->d_name);

			// Now that our paths are good, let's copy the file !
			copy_file(from, dest);
		}
		
		// NOTE: We can set errno ourselves, so we set it to 0
		// And now we check if it has changed, which means there was an error
		// while reading the directory entries
		if(errno != 0)
			perror("error while reading directory");

         	closedir(dir);
         }
}


int main(int argc, char** argv){
	if(argc < 3)
		printf("This program need at least 2 arguments");
	copy_file(argv[1], argv[2]);
	return 0;
}
